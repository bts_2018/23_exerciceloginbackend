

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import model.User;

/**
 * Servlet implementation class Login
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
 
	@SuppressWarnings("unchecked")
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//Permissions
		response.addHeader("Access-Control-Allow-Origin", "*");
		
		//variables
		boolean isConnect = false;
		JSONArray list = new JSONArray();
		JSONObject item = new JSONObject();
		
		//GET PARAMETER
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		
		
		try{
	 		//model 
	 		User user = new User();
	 		isConnect = user.Connect(username, password);
	 		
			if (isConnect == true) {
				item.put("connect",true);
			}else{
				item.put("connect",false);
			}
			list.add(item);
			
			//OUTPUT
			response.getWriter().append(list.toJSONString());
			
	 	}catch(Exception ex) {
	 		System.out.print(ex.getMessage());
	 	}
	 	
 
	}

 
	@SuppressWarnings("unchecked")
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//Permissions
		response.addHeader("Access-Control-Allow-Origin", "*");
		
		// CREATE JSON
		JSONArray list = new JSONArray();
		JSONObject item = new JSONObject();
		item.put("TYPE", "POST");
		item.put("username", request.getParameter("username"));
		item.put("password", request.getParameter("password"));
		list.add(item); 
	 			
		//OUTPUT
		response.getWriter().append(list.toJSONString());
	}

}
