package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;


public class User {
	private int idUser;
	private String username;
	private String password;
	private String token;
	
	private Connection conn = null;
	private Statement stmt = null;
	
	//Constructor
	public User()  {
		try {
			
			//Register Driver
			java.sql.DriverManager.registerDriver(new com.mysql.jdbc.Driver());
			
			//Connection
			this.conn = DriverManager.getConnection("jdbc:mysql://localhost/ecommerce?user=root&password=");

			//Create Statement
			this.stmt =  conn.createStatement();

		}catch(Exception ex) {
			System.out.println("error on CountryDAO constructor  : " + ex.getMessage());
		}
	}
	
	
	public int getIdUser() {
		return idUser;
	}
	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	
	public boolean Connect(String username, String password) {

		try {

			//SQL
			String sql = "SELECT COUNT(*) as count FROM users where username = '" +  username + "' and password='" +  password + "'";

			//Execute query
			ResultSet rs  = this.stmt.executeQuery(sql);
			rs.next();
			
			int count = rs.getInt("count");
			
			if (count == 1 ) 
				return true;
			else 
				return false;

		}catch (Exception ex) {
			System.out.println("error on Connect  : " + ex.getMessage());
			return false;
		}
	}
	
 
	
	
	
}
